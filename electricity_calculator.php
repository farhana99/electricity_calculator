<!DOCTYPE html>
<html>
<head>
    <title>Electricity Consumption Calculator</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>Electricity Consumption Calculator</h2>
        <form method="post" action="">
            <div class="form-group">
                <label for="voltage">Voltage (V):</label>
                <input type="number" class="form-control" id="voltage" name="voltage" required>
            </div>
            <div class="form-group">
                <label for="current">Current (A):</label>
                <input type="number" class="form-control" id="current" name="current" required>
            </div>
            <div class="form-group">
                <label for="hours">Hours:</label>
                <input type="number" class="form-control" id="hours" name="hours" required>
            </div>
            <div class="form-group">
                <label for="rate">Current Rate:</label>
                <input type="number" class="form-control" id="rate" name="rate" required>
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </form>

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Retrieve form data
            $voltage = $_POST['voltage'];
            $current = $_POST['current'];
            $hours = $_POST['hours'];
            $rate = $_POST['rate'];

            // Calculate power, energy, and total charge
            $power = $voltage * $current;
            $energy = $power * $hours / 1000;
            $totalCharge = $energy * ($rate / 100);

            // Display the results
            echo '<h3>Results:</h3>';
            echo 'Power (W): ' . $power . '<br>';
            echo 'Energy (kWh): ' . $energy . '<br>';
            echo 'Total Charge: RM' . $totalCharge . '<br>';
        }
        ?>
    </div>
</body>
</html>
